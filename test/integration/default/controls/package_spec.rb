# frozen_string_literal: true

control 'xdg-package-install-pkg-installed' do
  title 'should be installed'

  describe package('xdg-user-dirs') do
    it { should be_installed }
  end
end
