# frozen_string_literal: true

control 'xdg-config-file-user-auser-present' do
  title 'should be present'

  describe user('auser') do
    it { should exist }
  end
end

control 'xdg-config-file-xdg-dir-auser-managed' do
  title 'should exist'

  describe directory('/home/auser/.config') do
    it { should be_owned_by 'auser' }
    it { should be_grouped_into 'auser' }
    its('mode') { should cmp '0755' }
  end
end

control 'xdg-config-file-xdg-config-auser-managed' do
  title 'should match desired lines'

  describe file('/home/auser/.config/user-dirs.dirs') do
    it { should be_file }
    it { should be_owned_by 'auser' }
    it { should be_grouped_into 'auser' }
    its('mode') { should cmp '0644' }
    its('content') { should include('# Your changes will be overwritten.') }
    its('content') { should include('XDG_DESKTOP_DIR="$HOME/desktop"') }
    its('content') { should include('XDG_DOWNLOAD_DIR="$HOME/Downloads"') }
    its('content') { should include('XDG_TEMPLATES_DIR="$HOME/templates"') }
    its('content') { should include('XDG_PUBLICSHARE_DIR="$HOME/Public"') }
    its('content') { should include('XDG_DOCUMENTS_DIR="$HOME/documents"') }
    its('content') { should include('XDG_MUSIC_DIR="$HOME/Music"') }
    its('content') { should include('XDG_PICTURES_DIR="$HOME/pictures"') }
    its('content') { should include('XDG_VIDEOS_DIR="$HOME/Videos"') }
  end
end

control 'xdg-config-file-xdg-locale-config-auser-managed' do
  title 'should match desired lines'

  describe file('/home/auser/.config/user-dirs.locale') do
    it { should be_file }
    it { should be_owned_by 'auser' }
    it { should be_grouped_into 'auser' }
    its('mode') { should cmp '0644' }
    its('content') { should include('# Your changes will be overwritten.') }
    its('content') { should include('en_GB') }
  end
end
