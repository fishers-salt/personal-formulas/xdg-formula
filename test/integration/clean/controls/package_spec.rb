# frozen_string_literal: true

control 'xdg-package-clean-pkg-removed' do
  title 'should not be installed'

  describe package('xdg-user-dirs') do
    it { should_not be_installed }
  end
end
