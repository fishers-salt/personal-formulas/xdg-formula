# frozen_string_literal: true

control 'xdg-config-clean-xdg-config-auser-absent' do
  title 'should not exist'

  describe file('/home/auser/.config/user-dirs.dirs') do
    it { should_not exist }
  end
end

control 'xdg-config-clean-xdg-locale-config-auser-absent' do
  title 'should not exist'

  describe file('/home/auser/.config/user-dirs.locale') do
    it { should_not exist }
  end
end
