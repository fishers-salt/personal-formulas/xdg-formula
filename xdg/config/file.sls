# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- set sls_package_install = tplroot ~ '.package.install' %}
{%- from tplroot ~ "/map.jinja" import mapdata as xdg with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}
{%- from tplroot ~ "/libusers.jinja" import get_user_value_or_default %}

include:
  - {{ sls_package_install }}

{% if salt['pillar.get']('xdg-formula:use_users_formula', False) %}

{% for name, user in pillar.get('users', {}).items() if user.absent is not defined or not user.absent %}
{%- set current = salt.user.info(name) -%}
{%- if user == None -%}
{%- set user = {} -%}
{%- endif -%}
{%- set home = user.get('home', current.get('home', "/home/%s" % name)) -%}
{%- set manage = user.get('manage_xdg', False) -%}
{%- if 'prime_group' in user and 'name' in user['prime_group'] %}
{%- set user_group = user.prime_group.name -%}
{%- else -%}
{%- set user_group = name -%}
{%- endif %}
{%- if manage -%}

xdg-config-file-user-{{ name }}-present:
  user.present:
    - name: {{ name }}

xdg-config-file-xdg-dir-{{ name }}-managed:
  file.directory:
    - name: {{ home }}/.config
    - user: {{ name }}
    - group: {{ user_group }}
    - mode: '0755'
    - makedirs: True
    - require:
      - xdg-config-file-user-{{ name }}-present

{%- set desktop_dir = get_user_value_or_default(user, 'desktop_dir') %}
{%- set documents_dir = get_user_value_or_default(user, 'documents_dir') %}
{%- set download_dir = get_user_value_or_default(user, 'download_dir') %}
{%- set music_dir = get_user_value_or_default(user, 'music_dir') %}
{%- set pictures_dir = get_user_value_or_default(user, 'pictures_dir') %}
{%- set public_dir = get_user_value_or_default(user, 'public_dir') %}
{%- set template_dir = get_user_value_or_default(user, 'template_dir') %}
{%- set videos_dir = get_user_value_or_default(user, 'videos_dir') %}
xdg-config-file-xdg-config-{{ name }}-managed:
  file.managed:
    - name: {{ home }}/.config/user-dirs.dirs
    - source: {{ files_switch([
                  name ~ '-user-dirs.dirs.tmpl',
                  'user-dirs.dirs.tmpl'],
                lookup='xdg-config-file-xdg-config-' ~ name ~ '-managed',
                )
              }}
    - mode: '0644'
    - user: {{ name }}
    - group: {{ user_group }}
    - template: jinja
    - context:
        desktop_dir: $HOME/{{ desktop_dir }}
        documents_dir: $HOME/{{ documents_dir }}
        download_dir: $HOME/{{ download_dir }}
        music_dir: $HOME/{{ music_dir }}
        pictures_dir: $HOME/{{ pictures_dir }}
        public_dir: $HOME/{{ public_dir }}
        template_dir: $HOME/{{ template_dir }}
        videos_dir: $HOME/{{ videos_dir }}
    - require:
      - xdg-config-file-xdg-dir-{{ name }}-managed

xdg-config-file-xdg-locale-config-{{ name }}-managed:
  file.managed:
    - name: {{ home }}/.config/user-dirs.locale
    - source: {{ files_switch([
                  name ~ '-user-dirs.locale.tmpl',
                  'user-dirs.locale.tmpl'],
                lookup='xdg-config-file-xdg-locale-config-' ~ name ~ '-managed',
                )
              }}
    - mode: '0644'
    - user: {{ name }}
    - group: {{ user_group }}
    - template: jinja
    - require:
      - xdg-config-file-xdg-dir-{{ name }}-managed

{% endif %}
{% endfor %}
{% endif %}
